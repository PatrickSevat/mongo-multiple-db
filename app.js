const createClients = require('./createClients');
const createConnection = require('./db/mongoose').createConnection;
const getConnections = require('./db/mongoose').getConnections;
const configModel = require('./schema/config.model');
const createModels = require('./schema/client-schemas/index').createModels;
const _ = require('lodash');

const testConfig = {
  createClientConfig: true,
  createDocuments: true,
  retrieveDocuments: true,
};

const findClients = () => {
  return configModel
    .find({})
    .then((clients) => {
      console.log(clients.length + ' clients found \n');

      if (clients.length === 0 && testConfig.createClientConfig) {
        return createClients(Math.floor(Math.random() * 10) + 10)
          .then(() => Promise.reject('restart'))
      }

      console.log(`${clients.length} clients found: \n${clients}\n`);

      clients.map((client) => {
        createConnection(client.name)
      });
      return Promise.resolve()
    })
};

const createClientModels = () => {
  const clientConnections = getConnections(true);
  Object.keys(clientConnections).forEach((key) => {
    createModels(clientConnections[key].db)
  });

  console.log('models created \n');

  return Promise.resolve();
};

const createClientDocuments = () => {
  const creationPromises = [];

  if (testConfig.createDocuments) {
    const clientConnections = getConnections(true);
    Object.keys(clientConnections).forEach((key) => {
      const models = clientConnections[key].db.clientModels

      Object.keys(models).forEach((modelKey) => {
        const model = models[modelKey];
        for (let i = 0; i < 4; i++) {
          creationPromises.push(model.create({name: `${key}-${modelKey}-${i}`}))
        }
      })
    });
  }

  return Promise.all(creationPromises)
    .then((createdDocumentsArray) => {
      if (testConfig.createDocuments)
        console.log(createdDocumentsArray.length + 'documents created \n');

      return Promise.resolve()
    })
};

const retrieveDocuments = () => {
  const documentPromises = [];

  if (testConfig.retrieveDocuments) {
    const clientConnections = getConnections(true);
    Object.keys(clientConnections).forEach((key, ci) => {
      if (ci % 2 === 0) {
        const models = clientConnections[key].db.clientModels;

        Object.keys(models).forEach((modelKey, j) => {
          const model = models[modelKey];
          if (j % 2 === 0)
            documentPromises.push(model.find({}))
        })
      }
    });
  }

  return Promise.all(documentPromises);
};

const flattenDocuments = (documents) => {
  const flattened = _.flatten(documents);
  console.log(`${flattened.length} documents retrieved \n`);
  console.log(`documents retrieved[0]: ${flattened[0]}`);

  return Promise.resolve();
};

const startApp = () => {
  console.log('starting app \n');

  findClients()
    .then(createClientModels)
    .then(createClientDocuments)
    .then(retrieveDocuments)
    .then(flattenDocuments)
    .then(() => setTimeout(() => process.exit(0), 2000))
    .catch((err) => {
      if (err === 'restart') {
        return startApp();
      }

      console.log(err.stack);

      setTimeout(() => process.exit(0), 2000);
    })
};

startApp();