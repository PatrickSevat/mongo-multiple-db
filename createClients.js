const configModel = require('./schema/config.model');

const createClients = (numberOfClients) => {
  const promiseArr = [];

  for (let i = 0; i < numberOfClients; i++) {
    promiseArr.push(configModel.create({name: `naam-${i}-${Date.now()}`}))
  }

  return Promise.all(promiseArr)
    .then((res) => {
      console.log(`created ${res.length} clients \n`);
    });
};

module.exports = createClients;

