const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const connections = {
  config: {
    type: 'app',
    db: mongoose.createConnection('mongodb://localhost/multiple-test')
  }
};


const createConnection = name => {
  return connections[name] = {
    type: 'client',
    db: mongoose.createConnection(`mongodb://localhost/${name}`)
  };
};

const getConnections = (clientOnly) => {
  if (clientOnly) {
    const clients = {};

    Object.keys(connections).forEach((key) => {
      if (connections[key].type === 'client') {
        clients[key] = connections[key];
      }
    });
    return clients;
  }
  return connections
};

process.on('exit', () => {
  Object.keys(connections).forEach((key) => {
    console.log('closing connection ' + key);
    connections[key].db.close();
  })
});

exports.getConnections = getConnections;
exports.createConnection = createConnection;
exports.configDb = connections.config.db;