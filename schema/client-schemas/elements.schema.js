const mongoose = require('mongoose');

const ElementSchema = new mongoose.Schema({
  name: {
    type: String
  }
});

module.exports = ElementSchema;