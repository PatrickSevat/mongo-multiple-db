const glob = require('glob');
const path = require('path');

const schemas = {};

glob.sync('./**/*.schema.js').forEach((file) => {
  const parts = file.split('/');
  const name = parts[parts.length -1].split('.')[0];

  schemas[name] = require(path.resolve(file));
});

const createModels = db => {
  if (typeof db.clientModels === 'undefined') {
    db.clientModels = {};
  }

  if (typeof db.availableModels === 'undefined') {
    db.availableModels = [];
  }

  Object.keys(schemas).forEach((key) => {
    db.clientModels[key] = db.model(key, schemas[key]);
    db.availableModels.push(key)
  })
};

exports.schemas = schemas;
exports.createModels = createModels;