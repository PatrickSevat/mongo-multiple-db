const mongoose = require('mongoose');
const configDb = require('../db/mongoose').configDb;

// Schema
const Config = new mongoose.Schema({
  name: String,
  language: {
    type: String
  }
});

// Model
const ConfigModel = configDb.model('Config', Config);

module.exports = ConfigModel;